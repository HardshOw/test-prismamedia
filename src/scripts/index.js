import { qs } from './utils';

import '../stylesheets/style.scss';

parallaxe('.section-b-banner-img', 500);
parallaxe('.section-a-illustration', -500);
parallaxe('.section-a-content-text-wrapper', 1000);

function parallaxe(target, amount) {
  const el = qs(target);
  const parent = el.parentElement;
  window.addEventListener('scroll', function() {
    const start = parent.offsetTop;
    const end = parent.offsetTop + parent.offsetHeight + window.innerHeight;
    const isStart = window.scrollY + window.innerHeight > start;
    const isEnd = window.scrollY + window.innerHeight > end;
    // Parallax will work only when DOMElement is displayed according to the scroll level
    if (isStart && !isEnd) {
      el.style.transform = `translateY(${Math.round(((window.scrollY - start) * 100) / amount)}px)`;
    }
  });
}
