# Webpack ES6+ Sass Boilerplate [![devDependency Status](https://david-dm.org/vadimmarkov/webpack-es6-sass-boilerplate/dev-status.svg)](https://david-dm.org/vadimmarkov/webpack-es6-sass-boilerplate/?type=dev) [![Known Vulnerabilities](https://snyk.io/test/github/vadimmarkov/webpack-es6-sass-boilerplate/badge.svg)](https://snyk.io//test/github/vadimmarkov/webpack-es6-sass-boilerplate)

A minimalistic webpack 4 based boilerplate for building web apps.

## Commands

- `start` - start the dev server
- `build` - create build in `build` folder
- `analyze` - analyze your production bundle
- `lint-code` - run an ESLint check
- `lint-style` - run a Stylelint check
- `check-eslint-config` - check if ESLint config contains any rules that are unnecessary or conflict with Prettier
- `check-stylelint-config` - check if Stylelint config contains any rules that are unnecessary or conflict with Prettier
